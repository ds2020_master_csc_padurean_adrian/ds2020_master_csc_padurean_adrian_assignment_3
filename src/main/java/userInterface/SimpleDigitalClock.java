package userInterface;

import javax.swing.*;
import java.awt.*;
import java.util.Calendar;

class SimpleDigitalClock extends JPanel {

	public JFrame parentFrame;
	public static final int HOUR_TARGET =13;
	public static final int MINUTES_TARGET =07;
	String stringFormat;
	int hour_identify;
	int minute_identify;
	int second_identify;
	String aHour_identify = "";
	String bMinute_identify = "";
	String cSecond_identify = "";

	public String getStringFormat() {
		return stringFormat;
	}

	public int getHour_identify() {
		return hour_identify;
	}

	public SimpleDigitalClock setHour_identify(int hour_identify) {
		this.hour_identify = hour_identify;
		return this;
	}

	public int getMinute_identify() {
		return minute_identify;
	}

	public SimpleDigitalClock setMinute_identify(int minute_identify) {
		this.minute_identify = minute_identify;
		return this;
	}

	public int getSecond_identify() {
		return second_identify;
	}

	public SimpleDigitalClock setSecond_identify(int second_identify) {
		this.second_identify = second_identify;
		return this;
	}

	public String getaHour_identify() {
		return aHour_identify;
	}

	public SimpleDigitalClock setaHour_identify(String aHour_identify) {
		this.aHour_identify = aHour_identify;
		return this;
	}

	public String getbMinute_identify() {
		return bMinute_identify;
	}

	public SimpleDigitalClock setbMinute_identify(String bMinute_identify) {
		this.bMinute_identify = bMinute_identify;
		return this;
	}

	public String getcSecond_identify() {
		return cSecond_identify;
	}

	public SimpleDigitalClock setcSecond_identify(String cSecond_identify) {
		this.cSecond_identify = cSecond_identify;
		return this;
	}

	public void setStringFormat(String abc) {
		this.stringFormat = abc;
	}
	public int number(int a, int b) {
		return Math.min(a, b);
	}
	SimpleDigitalClock(JFrame parentFrame) {
		this.parentFrame=parentFrame;
		Timer t = new Timer(1000, e -> repaint());
		t.start();
	}
	@Override
	public void paintComponent(Graphics v) {
		super.paintComponent(v);
		Calendar cal = Calendar.getInstance();
		hour_identify = cal.get(Calendar.HOUR_OF_DAY);
		minute_identify = cal.get(Calendar.MINUTE);
		second_identify = cal.get(Calendar.SECOND);
		identifyDownloadTime();

		if (hour_identify < 10) {
			this.aHour_identify = "0";
		}
		if (hour_identify >= 10) {
			this.aHour_identify = "";
		}
		if (minute_identify < 10) {
			this.bMinute_identify = "0";
		}
		if (minute_identify >= 10) {
			this.bMinute_identify = "";
		}
		if (second_identify < 10) {
			this.cSecond_identify = "0";
		}
		if (second_identify >= 10) {
			this.cSecond_identify = "";
		}
		setString(v);
	}

	private void setString(Graphics v) {
		setStringFormat(aHour_identify + hour_identify + ":" + bMinute_identify + minute_identify + ":" + cSecond_identify + second_identify);
		v.setColor(Color.BLACK);
		int length = number(this.getWidth(), this.getHeight());
		Font font1 = new Font("SansSerif", Font.PLAIN, length / 5);
		v.setFont(font1);
		v.drawString(stringFormat, length / 6, length / 2);
	}

	private void identifyDownloadTime() {
		if(hour_identify == HOUR_TARGET && minute_identify == MINUTES_TARGET && second_identify == 0){
			((gRPCUi)parentFrame).downloadTask();
		}
	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(250, 250);
	}
}