package userInterface;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import server.*;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.ArrayList;
import java.util.Calendar;

public class medPanel extends JFrame{

	public medPanel(ArrayList<Medication> meds) {
		setLayout(new java.awt.GridLayout(2, 2));
		for (Medication med : meds) {
			for(Interval i:med.getIntervalList()){
				Dimension d = new Dimension(400, 100);
				String[] column = {"Nume Medicament", "Start Interval", "End Interval"};
				DefaultTableModel friendsTable = new DefaultTableModel(column, 0);
				JTable jt = new JTable(friendsTable);
				JScrollPane sp = new JScrollPane(jt);
				sp.setBounds(0, 0, 400, 300);
				sp.setPreferredSize(d);
				sp.setMaximumSize(d);
				jt.setBounds(0, 0, 400, 300);
				String name = med.getMedicationName();
				int start = i.getStartHour();
				int end = i.getEndHour();
				Object[] data = {name, start, end};
				friendsTable.addRow(data);
				add(sp);
				JButton b = new JButton("Take");
				b.setSize(50,50);
				add(b);
				b.addActionListener(new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent e) {
						Calendar cal = Calendar.getInstance();
						int hour;
						hour = cal.get(Calendar.HOUR_OF_DAY);
						if(hour >= i.getStartHour() && hour <= i.getEndHour()){
							System.out.println("MEDICAMENT LUAT CORECT");
							ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 8080)
									.usePlaintext()
									.build();

							MedicationPlanServiceGrpc.MedicationPlanServiceBlockingStub stubby = MedicationPlanServiceGrpc.newBlockingStub(channel);

							Status medReq=stubby.medicationTaken(
									MedTakenCorrenctly.newBuilder()
											.setMedName(med.getMedicationName())
											.setIntervalName(i.getIntervalName())
											.build()
							);
							if(medReq.getStatusCode()==200){
								sp.setVisible(false);
								b.setVisible(false);
							}
							System.out.println(medReq);

							channel.shutdown();

							ManagedChannel channely = ManagedChannelBuilder.forAddress("localhost", 8080)
									.usePlaintext()
									.build();

							MedicationPlanServiceGrpc.MedicationPlanServiceBlockingStub stubby2 = MedicationPlanServiceGrpc.newBlockingStub(channely);

							MedicationPlanRequest getMedicationPlanRequest=stubby2.getMedicationPlanForPatient(
									GetMedicationPlanRequest.newBuilder().setPatientId(1).build()
							);

							ArrayList<Medication> medList=new ArrayList<Medication>(getMedicationPlanRequest.getMedicationList());

							System.out.println("Response received from server:\n" + getMedicationPlanRequest);

							channely.shutdown();
						}else{
							System.out.println("MEDICAMENT NE LUAT CORECT");
						}
					}
				});
			}
		}
	}
}
