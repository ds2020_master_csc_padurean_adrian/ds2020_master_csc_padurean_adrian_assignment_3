package userInterface;

import com.opencsv.CSVWriter;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import server.*;

import static javax.swing.JOptionPane.showMessageDialog;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

class gRPCUi extends JFrame{
	static DefaultTableModel friendsTable;
	public static JFrame frame;

	public gRPCUi(){
		this.setTitle("Personal Medication Dispenser");
		this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		Dimension d=new Dimension(400,400);
		this.setMinimumSize(d);
		JFrame.setDefaultLookAndFeelDecorated(true);

		JButton getMedPlan = new JButton("Get Medication Plan");

		JLabel jl=new JLabel("Current Time:");
		SimpleDigitalClock clock1 = new SimpleDigitalClock(this);


		getMedPlan.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 8080)
						.usePlaintext()
						.build();

				MedicationPlanServiceGrpc.MedicationPlanServiceBlockingStub stubby = MedicationPlanServiceGrpc.newBlockingStub(channel);

				MedicationPlanRequest getMedicationPlanRequest=stubby.getMedicationPlanForPatient(
						GetMedicationPlanRequest.newBuilder().setPatientId(1).build()
				);

				ArrayList<Medication> medList=new ArrayList<Medication>(getMedicationPlanRequest.getMedicationList());

				System.out.println("Response received from server:\n" + getMedicationPlanRequest);

				channel.shutdown();

				medPanel newFrame=new medPanel(medList);
				newFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				newFrame.setSize(new Dimension(800, 300));
				newFrame.setVisible(true);
			}
		});

		this.getContentPane().add(clock1, BorderLayout.NORTH);
		this.getContentPane().add(getMedPlan, BorderLayout.SOUTH);
		this.setLocationRelativeTo(null);
		this.pack();

		this.setVisible(true);
	}

	public static void main(String args[]) {
		gRPCUi g=new gRPCUi();
	}

	private static void populateFriendsTable(ArrayList<Medication> meds) {
		for (Medication med : meds) {
			for(Interval i:med.getIntervalList()){
				String name = med.getMedicationName();
				int start = i.getStartHour();
				int end = i.getEndHour();
				Object[] data = {name, start, end};
				friendsTable.addRow(data);
			}
		}
	}

	public void downloadTask() {
		ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 8080)
				.usePlaintext()
				.build();

		MedicationPlanServiceGrpc.MedicationPlanServiceBlockingStub stubby = MedicationPlanServiceGrpc.newBlockingStub(channel);

		MedicationPlanRequest getMedicationPlanRequest=stubby.getMedicationPlanForPatient(
				GetMedicationPlanRequest.newBuilder().setPatientId(1).build()
		);

		ArrayList<Medication> medList=new ArrayList<Medication>(getMedicationPlanRequest.getMedicationList());

		String filePath="output.csv";
		File file = new File(filePath);

		try {
			FileWriter outputfile = new FileWriter(file);
			CSVWriter writer = new CSVWriter(outputfile);
			ArrayList<String[]> data = new ArrayList<String[]>();
			data.add(new String[] { "Nume Medicament", "Start Date", "End Date" });
			for (Medication med : medList) {
				for(Interval i:med.getIntervalList()){
					String name = med.getMedicationName();
					int start = i.getStartHour();
					int end = i.getEndHour();
					data.add(new String[]{name,String.valueOf(start), String.valueOf(end)});
				}
			}
			writer.writeAll(data);

			writer.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		this.showMessageBox();
	}

	public void showMessageBox() {
		JFrame frame = new JFrame("Show Message Box");
		JLabel label = new JLabel("Medication Plan Downloaded");
		label.setAlignmentX((float) 100.00);

		frame.add(label);
		frame.setLocationRelativeTo(null);
		JFrame.setDefaultLookAndFeelDecorated(true);
		frame.setSize(300, 100);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}


}
