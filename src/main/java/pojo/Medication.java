package pojo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class Medication {
	public String medicationName;
	public List<Interval> interval=new ArrayList<>();

	public Medication() {
	}

	public Medication(String medicationName, List<Interval> interval) {
		this.medicationName = medicationName;
		this.interval = interval;
	}

	public String getMedicationName() {
		return medicationName;
	}

	public Medication setMedicationName(String medicationName) {
		this.medicationName = medicationName;
		return this;
	}

	public List<Interval> getInterval() {
		return interval;
	}

	public Medication setInterval(List<Interval> interval) {
		this.interval = interval;
		return this;
	}

	public Medication addToIntervalList(Interval i){
		this.interval.add(i);
		return this;
	}

	public boolean removeInterval(Interval i){
		Iterator itr = this.interval.iterator();
		while (itr.hasNext())
		{
			if (((Interval)itr.next()).getIntervalName() .equals(i.getIntervalName())) {
				itr.remove();
				return true;
			}
		}
		return false;
	}

}
