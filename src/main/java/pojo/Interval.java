package pojo;

public class Interval {
	public String intervalName;
	public int startHour;
	public int endHour;

	public int getEndHour() {
		return endHour;
	}

	public Interval setEndHour(int endHour) {
		this.endHour = endHour;
		return this;
	}

	public Interval() {
	}

	public Interval(String intervalName, int startHour, int endHour) {
		this.intervalName = intervalName;
		this.startHour = startHour;
		this.endHour = endHour;
	}

	public String getIntervalName() {
		return intervalName;
	}

	public Interval setIntervalName(String intervalName) {
		this.intervalName = intervalName;
		return this;
	}

	public int getStartHour() {
		return startHour;
	}

	public Interval setStartHour(int startHour) {
		this.startHour = startHour;
		return this;
	}
}
