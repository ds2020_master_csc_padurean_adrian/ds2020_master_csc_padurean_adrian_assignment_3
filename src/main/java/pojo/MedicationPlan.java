package pojo;

import java.util.ArrayList;
import java.util.List;

public class MedicationPlan {
	public List<Medication> meds=new ArrayList<>();

	public MedicationPlan() {
	}

	public MedicationPlan(List<Medication> meds) {
		this.meds = meds;
	}

	public List<Medication> getMeds() {
		return meds;
	}

	public MedicationPlan setMeds(List<Medication> meds) {
		this.meds = meds;
		return this;
	}

	public MedicationPlan addMedToPlan(Medication m){
		this.meds.add(m);
		return this;
	}
}
