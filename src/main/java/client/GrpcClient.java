package client;

import server.*;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

import java.util.ArrayList;

public class GrpcClient {
    public static void main(String[] args) throws InterruptedException {

        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 8080)
                .usePlaintext()
                .build();

        MedicationPlanServiceGrpc.MedicationPlanServiceBlockingStub stubby = MedicationPlanServiceGrpc.newBlockingStub(channel);

        MedicationPlanRequest getMedicationPlanRequest=stubby.getMedicationPlanForPatient(
                GetMedicationPlanRequest.newBuilder().setPatientId(1).build()
        );

        ArrayList<Medication> medList=new ArrayList<Medication>(getMedicationPlanRequest.getMedicationList());

        System.out.println("Response received from server:\n" + getMedicationPlanRequest);

        channel.shutdown();

        channel.shutdown();
    }
}
