package server;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import pojo.Interval;
import pojo.Medication;
import pojo.MedicationPlan;

import java.io.IOException;

public class GrpcServer {
    public static MedicationPlan dummyMedicationPlan;

    public static void main(String[] args) throws IOException, InterruptedException {
        Server server = ServerBuilder.forPort(8080)
          .addService(new MedicationPlanService()).build();


        Interval inty=new pojo.Interval()
                .setIntervalName("Morning")
                .setStartHour(6)
                .setEndHour(17);
        Interval intyy=new pojo.Interval()
                .setIntervalName("Evening")
                .setStartHour(18)
                .setEndHour(24);

        Medication m=new pojo.Medication()
                .setMedicationName("FLUOROURACILUM")
                .addToIntervalList(inty)
                .addToIntervalList(intyy);

        dummyMedicationPlan=new MedicationPlan()
                .addMedToPlan(m);

        System.out.println("Starting server...");
        server.start();
        System.out.println("Server started!");
        server.awaitTermination();
    }


}
