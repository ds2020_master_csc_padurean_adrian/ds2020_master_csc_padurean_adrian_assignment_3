package server;


import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;

public class MedicationPlanService extends MedicationPlanServiceGrpc.MedicationPlanServiceImplBase {

    @Override
    public void getMedicationPlan(MedicationPlanRequest request, StreamObserver<server.Status> responseObserver) {
        System.out.println("Request received from client:\n" + request);

        StringBuilder greeting = new StringBuilder().append("Your medication plan is: ");

//        for (int x=0; x<request.getMedsCount(); x++){
//            greeting
//                    .append("\n")
//                    .append(" pojo.Medication name: ")
//                    .append(request.getMeds(x).getMedicationName())
//                    .append(" Med interval name: ")
//                    .append(request.getMeds(x).getInterval().getIntervalName())
//                    .append(" Med interval time: ")
//                    .append(request.getMeds(x).getInterval().getFrequency());
//        }


        String greetings = greeting.toString();

        server.Status response = Status.newBuilder().setStatus(greetings).setStatusCode(200).build();

        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void getMedicationPlanForPatient(GetMedicationPlanRequest request, StreamObserver<MedicationPlanRequest> responseObserver) {
        System.out.println("Request received from client:\n" + request);
        MedicationPlanRequest.Builder br = MedicationPlanRequest.newBuilder();
        for (pojo.Medication mm : GrpcServer.dummyMedicationPlan.getMeds()) {
            Medication.Builder mb = Medication.newBuilder();
            mb.setMedicationName(mm.getMedicationName());
                for (pojo.Interval interval : mm.getInterval()) {
                    Interval i = Interval.newBuilder()
                            .setIntervalName(interval.getIntervalName())
                            .setStartHour(interval.getStartHour())
                            .setEndHour(interval.getEndHour())
                            .build();
                    mb.addInterval(i);
                }
            mb.build();
            br.addMedication(mb);
        }
        br.setPatientId(request.getPatientId());
        MedicationPlanRequest response=br.build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void medicationTaken(MedTakenCorrenctly request, StreamObserver<Status> responseObserver){
        System.out.println("Request received from client:\n" + request);

        Status.Builder br = Status.newBuilder();

        br.setStatusCode(404);
        br.setStatus("not found");
        for(pojo.Medication m: GrpcServer.dummyMedicationPlan.getMeds()){
            if(m.getMedicationName().equals(request.getMedName())){
                for(pojo.Interval i: m.getInterval()){
                    if(i.getIntervalName().equals(request.getIntervalName())){
                        m.removeInterval(i);
                        br.setStatusCode(200);
                        br.setStatus("found and deleted");
                    }
                }
            }
        }
        responseObserver.onNext(br.build());
        responseObserver.onCompleted();
    }
}
